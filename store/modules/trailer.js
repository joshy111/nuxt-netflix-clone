export default {
    namespaced: true,
    state:  {
        trailer : {
            ID: null,
            showing: false,
            data: {videoURL: null, name:'', desc: '', hasVideo: null, img: null},
        }
    },
    mutations: {
        setDetails(state, payload) {
            //assign ID
            state.trailer.ID = payload.id
            //change state
            state.trailer.showing = true
            //assign data
            // state.trailer.data.videoURL = "https://www.youtube.com/embed/0NTXawVhIww"
            state.trailer.data.img = "https://image.tmdb.org/t/p/w500"+payload.img
            state.trailer.data.hasVideo = payload.video
            state.trailer.data.name = payload.title
            state.trailer.data.desc = payload.desc
        },
        closeTrailerModal(state, payload) {
            if(state.trailer.ID != payload){
                return
            }
            state.trailer.showing = false
        }
    },
    getters: {
        trailerData(state){
            return state.trailer
        },
        isShowing(state){
            return state.trailer.showing
        }
    }
}